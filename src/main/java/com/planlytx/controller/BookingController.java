package com.planlytx.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.planlytx.dto.BookingAcknowledgement;
import com.planlytx.dto.BookingRequest;
import com.planlytx.service.BookingService;

@RestController
public class BookingController {

	@Autowired
	private BookingService bookingService;

	@RequestMapping("/getAcknowledgement")
	public BookingAcknowledgement bookTicket(@RequestBody BookingRequest bookingRequest) throws Exception {
		return bookingService.bookTicket(bookingRequest);
	}
}
