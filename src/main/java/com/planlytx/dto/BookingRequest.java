package com.planlytx.dto;

import com.planlytx.entity.PassengerInformation;
import com.planlytx.entity.PaymentInfo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BookingRequest {

	private PassengerInformation passengerInformation;
	private PaymentInfo paymentInfo;

	public PassengerInformation getPassengerInformation() {
		return passengerInformation;
	}

	public void setPassengerInformation(PassengerInformation passengerInformation) {
		this.passengerInformation = passengerInformation;
	}

	public PaymentInfo getPaymentInfo() {
		return paymentInfo;
	}

	public void setPaymentInfo(PaymentInfo paymentInfo) {
		this.paymentInfo = paymentInfo;
	}

}
