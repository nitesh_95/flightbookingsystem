package com.planlytx.dto;

import com.planlytx.entity.PassengerInformation;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BookingAcknowledgement {

	private String status;
	private Double totalfare;
	private String pnrno;
	private PassengerInformation passengerInformation;

	public BookingAcknowledgement(String status, Double totalfare, String pnrno,
			PassengerInformation passengerInformation) {
		super();
		this.status = status;
		this.totalfare = totalfare;
		this.pnrno = pnrno;
		this.passengerInformation = passengerInformation;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Double getTotalfare() {
		return totalfare;
	}

	public void setTotalfare(Double totalfare) {
		this.totalfare = totalfare;
	}

	public String getPnrno() {
		return pnrno;
	}

	public void setPnrno(String pnrno) {
		this.pnrno = pnrno;
	}

	public PassengerInformation getPassengerInformation() {
		return passengerInformation;
	}

	public void setPassengerInformation(PassengerInformation passengerInformation) {
		this.passengerInformation = passengerInformation;
	}

}
