package com.planlytx.util;

import java.util.HashMap;
import java.util.Map;

public class PaymentUtils {

	private static Map<String, Double> paymentMap = new HashMap<>();

	static {
		paymentMap.put("SBI_CARD", 20000.0);
		paymentMap.put("CHARTERED_Bank", 12000.0);
		paymentMap.put("RBL_CARD", 5000.0);
		paymentMap.put("KOTAK_CARD", 8000.0);
		paymentMap.put("HDFC_BANK", 12000.0);
	}

	public static boolean validateAmountInBank(String cardName, double totalfare) throws Exception {

		Double double1 = paymentMap.get(cardName);
		if (totalfare > double1) {
			throw new Exception("The fund is not sufficient");
		} else {
			return true;
		}
	}
}