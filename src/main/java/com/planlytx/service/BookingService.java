package com.planlytx.service;

import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.planlytx.dto.BookingAcknowledgement;
import com.planlytx.dto.BookingRequest;
import com.planlytx.entity.PassengerInformation;
import com.planlytx.entity.PaymentInfo;
import com.planlytx.repository.PaymentDetails;
import com.planlytx.repository.TravelRepo;
import com.planlytx.util.PaymentUtils;

@Service
public class BookingService {
	@Autowired
	private PaymentDetails paymentDetails;
	@Autowired
	private TravelRepo travelRepo;

	@Transactional
	public BookingAcknowledgement bookTicket(BookingRequest request) throws Exception {
		System.out.println("******************************************");
		PassengerInformation passengerInformation = request.getPassengerInformation();
		System.out.println(passengerInformation);
		travelRepo.save(passengerInformation);
		PaymentInfo paymentInfo = request.getPaymentInfo();
		PaymentUtils.validateAmountInBank(paymentInfo.getCardname(), passengerInformation.getFare());
		paymentInfo.setPassengerid(passengerInformation.getPid());
		paymentInfo.setAmount(passengerInformation.getFare());
		paymentDetails.save(paymentInfo);
		BookingAcknowledgement bookingAcknowledgement2 = new BookingAcknowledgement("SUCCESS",
				passengerInformation.getFare(), UUID.randomUUID().toString().split("-")[0], passengerInformation);
		return bookingAcknowledgement2;
	}

}
