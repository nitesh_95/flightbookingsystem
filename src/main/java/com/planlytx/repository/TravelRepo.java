package com.planlytx.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.planlytx.entity.PassengerInformation;

@Repository
public interface TravelRepo extends JpaRepository<PassengerInformation, Long> {

}
