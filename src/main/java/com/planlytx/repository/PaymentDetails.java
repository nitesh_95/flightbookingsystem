package com.planlytx.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.planlytx.entity.PaymentInfo;
@Repository
public interface PaymentDetails extends JpaRepository<PaymentInfo, Integer> {

}
